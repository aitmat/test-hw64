<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Phrase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $addButton = $this->get('translator')->trans('add');
        $form = $this->createFormBuilder()
            ->add('ruName', TextType::class, ['label' => false])
            ->add('add', SubmitType::class, ['label' => $addButton])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $phrase = new Phrase();
            $phrase->translate('ru')->setName($data['ruName']);

            $em = $this->getDoctrine()->getManager();
            $em->persist($phrase);

            $phrase->mergeNewTranslations();
            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        $phrases = $this->getDoctrine()->getRepository('AppBundle:Phrase')->findAll();

        return $this->render('default/index.html.twig', [
            'addPhrase' => $form->createView(),
            'phrases' => $phrases,
        ]);
    }

    /**
     * @Route("/translation/{id}", name="translations", requirements={"id": "\d+"})
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function translationsAction(Request $request, int $id)
    {
        $addButton = $this->get('translator')->trans('add');
        $form = $this->createFormBuilder()
            ->add('ruName', TextType::class, ['label' => false, 'required' => false])
            ->add('enName', TextType::class, ['label' => false, 'required' => false])
            ->add('kgName', TextType::class, ['label' => false, 'required' => false])
            ->add('kzName', TextType::class, ['label' => false, 'required' => false])
            ->add('frName', TextType::class, ['label' => false, 'required' => false])
            ->add('add', SubmitType::class, ['label' => $addButton])
            ->getForm();

        $phrase = $this->getDoctrine()->getRepository('AppBundle:Phrase')->find($id);


        $form->handleRequest($request);

        if($form->isSubmitted()) {
            $data = $form->getData();

            if ($data['frName'] !== null){
                $phrase->translate('fr', false)->setName($data['frName']);
            }
            if ($data['kzName'] !== null){
                $phrase->translate('kz', false)->setName($data['kzName']);
            }
            if ($data['kgName'] !== null){
                $phrase->translate('kg', false)->setName($data['kgName']);
            }
            if ($data['enName'] !== null){
                $phrase->translate('en', false)->setName($data['enName']);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($phrase);

            $phrase->mergeNewTranslations();
            $em->flush();

            return $this->redirectToRoute('translations', ['id' => $phrase->getId()]);
        }

        $array = [
                'id' => $phrase->getId(),
                'ruName' => $phrase->translate('ru', false)->getName() ?: false,
                'enName' => $phrase->translate('en', false)->getName() ?: false,
                'kgName' => $phrase->translate('kg', false)->getName() ?: false,
                'kzName' => $phrase->translate('kz', false)->getName() ?: false,
                'frName' => $phrase->translate('fr', false)->getName() ?: false,
            ];


        return $this->render('default/translation.html.twig', [
            'translations' => $array,
            'addTranslate' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{_locale}", name="locale", requirements = {"_locale" : "en|ru"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function changeLocaleAction(Request $request)
    {
        return $this->redirect($request->server->get('HTTP_REFERER'));
    }
}
